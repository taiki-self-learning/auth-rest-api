package com.learning.authentication.security;

import static java.util.stream.Collectors.joining;

import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.learning.authentication.security.dto.AuthRestResponse;
import com.learning.authentication.security.dto.CommonDto;
import com.learning.authentication.security.dto.ResultDto;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.web.WebAttributes;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

import lombok.extern.slf4j.Slf4j;

/**
 * 認証が成功した時の処理
 */
@Slf4j
public class SimpleAuthenticationSuccessHandler implements AuthenticationSuccessHandler {

    private ObjectMapper objectMapper;

    @Override
    public void onAuthenticationSuccess(HttpServletRequest req, HttpServletResponse res, Authentication auth)
            throws IOException, ServletException {
        if (res.isCommitted()) {
            log.info("Response has already been committed.");
            return;
        }

        // HTTPステータス設定
        res.setStatus(HttpStatus.OK.value());

        // Body設定
        res.setContentType(MediaType.APPLICATION_JSON_VALUE);
        res.setCharacterEncoding(StandardCharsets.UTF_8.name());
        PrintWriter out = res.getWriter();
        out.print(this.createBodyJson(req, auth));
        out.flush();
        out.close();

        // 認証プロセス中にセッションに保存された一時的な認証関連データを削除します。
        this.clearAuthenticationAttributes(req);
    }

    private String createBodyJson(HttpServletRequest req, Authentication auth) throws JsonProcessingException {
        this.objectMapper = new ObjectMapper();
        AuthRestResponse authRestResponse = new AuthRestResponse(
            new CommonDto(new ResultDto("0")),
            req.getParameterValues("id")[0],
            auth.getName(),
            auth.getAuthorities().stream().map(GrantedAuthority::getAuthority).collect(joining("\n  "))
        );
        return this.objectMapper.writeValueAsString(authRestResponse);
    }

    private void clearAuthenticationAttributes(HttpServletRequest req) {
        HttpSession session = req.getSession(false);
        if (session == null) {
            return;
        }
        session.removeAttribute(WebAttributes.AUTHENTICATION_EXCEPTION);
    }
}