package com.learning.authentication.security;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.logout.HttpStatusReturningLogoutSuccessHandler;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;

@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        // 静的リソース(images、css、javascript)に対するアクセスはセキュリティ設定を無視する
        http
            .authorizeRequests()
                .antMatchers("/prelogin")
                    .permitAll()
                .anyRequest()
                    .authenticated()
            .and()

            // 例外
            .exceptionHandling()
                .authenticationEntryPoint(this.authenticationEntryPoint())
                .accessDeniedHandler(this.accessDeniedHandler())
            .and()

            // ログイン設定
            .formLogin()
                .loginProcessingUrl("/login").permitAll()
                    .usernameParameter("id")
                    .passwordParameter("pass")
                .successHandler(this.authenticationSuccessHandler())
                .failureHandler(this.authenticationFailureHandler())
            .and()

            // ログアウト設定
            .logout()
                .logoutUrl("/logout")
                .invalidateHttpSession(true)
                .deleteCookies("JSESSIONID")
                .logoutSuccessHandler(this.logoutSuccessHandler())
            .and()

            // CSRF
            .csrf()
                .disable();
    }

    public void configureGlobal(AuthenticationManagerBuilder auth,
            @Qualifier("simpleUserDetailsService") UserDetailsService userDetailsService,
            PasswordEncoder passwordEncoder) throws Exception {
        auth.eraseCredentials(true)
            .userDetailsService(userDetailsService)
            .passwordEncoder(passwordEncoder);
    }

    @Bean
    PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    private AuthenticationEntryPoint authenticationEntryPoint() {
        return new SimpleAuthenticationEntryPoint();
    }

    private AccessDeniedHandler accessDeniedHandler() {
        return new SimpleAccessDeniedHandler();
    }

    private AuthenticationSuccessHandler authenticationSuccessHandler() {
        return new SimpleAuthenticationSuccessHandler();
    }

    private AuthenticationFailureHandler authenticationFailureHandler() {
        return new SimpleAuthenticationFailureHandler();
    }

    private LogoutSuccessHandler logoutSuccessHandler() {
        return new HttpStatusReturningLogoutSuccessHandler();
    }
}