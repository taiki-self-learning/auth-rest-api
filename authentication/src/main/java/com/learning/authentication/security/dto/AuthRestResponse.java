package com.learning.authentication.security.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class AuthRestResponse {

    /** 共通DTO */
    private CommonDto commondto;

    /** ユーザID */
    private String userid;

    /** ユーザ名 */
    private String username;

    /** 権限 */
    private String role;
}