package com.learning.authentication.infrastructure.repository;

import com.learning.authentication.domain.model.AuthModel;
import com.learning.authentication.domain.repository.AuthRepository;
import com.learning.authentication.infrastructure.entity.UserInfoEntity;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Repository;

/**
 * 認証情報を利用するためのインターフェース。
 */
@Repository
@RequiredArgsConstructor
public class AuthRepositoryImpl implements AuthRepository {

    @NonNull
    private final UserInfoJpaRepository userInfoJpaRepository;

    /**
     * {@inheritDoc}
     */
    @Override
    public AuthModel findById(String id) throws UsernameNotFoundException {

        // 認証情報取得
        UserInfoEntity entity =
            this.userInfoJpaRepository.findById(id).orElseThrow(
                () -> new UsernameNotFoundException("user not found")
            );

        return this.entity2AuthModel(entity);
    }

    /**
     * 認証情報ドメインモデル変換。
     * 
     * @param entity 認証情報エンティティ
     * @return 認証情報ドメインモデル
     */
    private AuthModel entity2AuthModel(UserInfoEntity entity) {
        return new AuthModel(
            entity.getId(),
            entity.getUserDetailEntity().getUsername(),
            entity.getUserDetailEntity().getPassword(),
            entity.getUserDetailEntity().getEmail(),
            entity.getAuthority()
        );
    }
}
