package com.learning.authentication.infrastructure.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * ユーザ詳細テーブルエンティティクラス。
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "user_detail")
public class UserDetailEntity implements Serializable {

    /** serialVersionUID  */
    private static final long serialVersionUID = 1L;

    /** id */
    @Id
    @Column(name = "user_id", nullable = false, unique = true)
    private String userId;

    /** ユーザ名 */
    @Column(name = "user_name", nullable = false)
    private String username;

    /** パスワード */
    @Column(nullable = false)
    private String password;

    /** メールアドレス */
    @Column
    private String email;
}
