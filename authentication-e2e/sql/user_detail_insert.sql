insert into user_detail(user_id,user_name,password,email,created_at,updated_at)
values 
('999999','admin','$2a$10$/EhB6vOBQuKs2lcaXJpoz.7QmyFTELqssswE7s2K/pJ3.DlvK4.QO','admin@localhost',current_timestamp,current_timestamp)
,('000001','user','$2a$10$aIx7BBFv0k7PqcldSzuQaum2WcoQosWjWMzQUKP1XNSZfhuwu8W1m','user@localhost',current_timestamp,current_timestamp);