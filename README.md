# 認証用RestAPI

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->


- [概要](#%E6%A6%82%E8%A6%81)
- [はじめに](#%E3%81%AF%E3%81%98%E3%82%81%E3%81%AB)
- [事前準備](#%E4%BA%8B%E5%89%8D%E6%BA%96%E5%82%99)
    - [プロジェクトクローン](#%E3%83%97%E3%83%AD%E3%82%B8%E3%82%A7%E3%82%AF%E3%83%88%E3%82%AF%E3%83%AD%E3%83%BC%E3%83%B3)
    - [Homebrewインストール](#homebrew%E3%82%A4%E3%83%B3%E3%82%B9%E3%83%88%E3%83%BC%E3%83%AB)
    - [PostgreSQLインストール](#postgresql%E3%82%A4%E3%83%B3%E3%82%B9%E3%83%88%E3%83%BC%E3%83%AB)
    - [ユーザ 及び データベース作成](#%E3%83%A6%E3%83%BC%E3%82%B6-%E5%8F%8A%E3%81%B3-%E3%83%87%E3%83%BC%E3%82%BF%E3%83%99%E3%83%BC%E3%82%B9%E4%BD%9C%E6%88%90)
    - [テーブル 及び データ作成](#%E3%83%86%E3%83%BC%E3%83%96%E3%83%AB-%E5%8F%8A%E3%81%B3-%E3%83%87%E3%83%BC%E3%82%BF%E4%BD%9C%E6%88%90)
    - [wildfly準備](#wildfly%E6%BA%96%E5%82%99)
    - [yarnインストール](#yarn%E3%82%A4%E3%83%B3%E3%82%B9%E3%83%88%E3%83%BC%E3%83%AB)
    - [node_modulesインストール](#node_modules%E3%82%A4%E3%83%B3%E3%82%B9%E3%83%88%E3%83%BC%E3%83%AB)
- [使い方](#%E4%BD%BF%E3%81%84%E6%96%B9)
    - [サーバ起動](#%E3%82%B5%E3%83%BC%E3%83%90%E8%B5%B7%E5%8B%95)
    - [動作確認](#%E5%8B%95%E4%BD%9C%E7%A2%BA%E8%AA%8D)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## 概要

認証用API。
本プロジェクトには下記機能が含まれています。
 - 認証機能（Spring Security）

## はじめに

本プロジェクトはMac OS XまたはLinuxで使用することを目的としています。
また、以下の手順はMac OS X専用です。


## 事前準備

### プロジェクトクローン

```bash
$ git clone https://gitlab.com/taiki-self-learning/auth-rest-api.git 
```

### Homebrewインストール

```bash
$ /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"  
```

### PostgreSQLインストール

```bash
$ brew install postgresql 
$ brew services start postgresql  
```

### ユーザ 及び データベース作成

```bash
$ psql postgres 
postgres=# CREATE ROLE admin LOGIN PASSWORD 'Defaultp@ss';
postgres=# CREATE DATABASE auth;
postgres=# \q
```

### テーブル 及び データ作成

```bash
$ cd 本プロジェクトパス/kinda-e2e/sql
$ psql -U admin -d auth 
auth=# \i create_user_detail.sql
auth=# \i create_user_info.sql
auth=# \i user_detail_insert.sql 
auth=# \i user_info_insert.sql 
auth=# \q
```

### wildfly準備

下記URLからpostgresql-42.2.8.jarをダウンロード

```
https://jdbc.postgresql.org/download/postgresql-42.2.8.jar
```

postgresql-42.2.8.jarを下記に配置

```
auth-rest-api/authentication-e2e/wildfly/standalone/deployments
```

### yarnインストール

```bash
$ brew install yarn --ignore-dependencies
```

### node_modulesインストール

```bash
$ yarn install
```

## 使い方

### サーバ起動

```bash
$ cd 本プロジェクトパス/authentication-e2e
$ yarn start
```

### 動作確認

```bash
$ cd 本プロジェクトパス/authentication-e2e
$ yarn test:login
```